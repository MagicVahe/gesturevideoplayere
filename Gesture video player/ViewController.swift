//
//  ViewController.swift
//  Gesture video player
//
//  Created by Vahe on 20.03.22.
//

import UIKit
import AVKit
import CoreLocation
import CoreMotion

class ViewController: UIViewController {
    
    // MARK: - Variables
    private let urlPath = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4"
    private var player: AVPlayer!
    private var playerController = AVPlayerViewController()
    private var startLocation: CLLocation!
    private let manager = CMMotionManager()
    private let replayVideoStepInMeters: Double = 10
    private let seekDegrees: Double = 30
    private let volumeDegrees: Double = 30
    private let seekStep = 5
    private let volumeStep: Float = 0.1
    lazy var locationManager:CLLocationManager! = {
        let manager = CLLocationManager()
        manager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        manager.delegate = self
        manager.requestAlwaysAuthorization()
        manager.pausesLocationUpdatesAutomatically = false
        return manager
    }()
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        manager.deviceMotionUpdateInterval = 0.5
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForegroundNotification), name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackgroundNotification), name: UIApplication.didEnterBackgroundNotification, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        playVideo(urlPath)
        startLocationUpdate()
        startDeviceMotionUpdates()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func startDeviceMotionUpdates() {
        manager.startDeviceMotionUpdates(to: .main) { [self] (motion, error) in
            guard let deviceMotion = manager.deviceMotion else {
                return
            }
            let attitude = deviceMotion.attitude
            let yawInDegrees = attitude.yaw * 180 / .pi
            let pitchInDegrees = attitude.pitch * 180 / .pi
            if yawInDegrees > seekDegrees {
                seekVideo(value: -seekStep)
            } else if yawInDegrees < -seekDegrees {
                seekVideo(value: seekStep)
            }
            if pitchInDegrees > volumeDegrees {
                playerController.player?.volume = max(0, playerController.player!.volume - volumeStep)
            } else if pitchInDegrees < -volumeDegrees {
                playerController.player?.volume =  min(1, playerController.player!.volume + volumeStep)
            }
        }
    }
    
    // MARK: - ShakeDevice
    override func motionBegan(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            if playerController.player!.timeControlStatus == .playing {
                pauseVideo()
            }
        }
    }
    
    // MARK: - Configuration
    func startLocationUpdate() {
        locationManager.startUpdatingLocation()
    }
    
    func playVideo(_ url: String) {
        if let videoUrl = URL(string: url) {
            player = AVPlayer(url: videoUrl)
            playerController.player = player
            playerController.showsPlaybackControls = false
            self.present(playerController, animated: true) { self.playerController.player?.play() }
            NotificationCenter.default.addObserver(self, selector: #selector(videoDidEnded), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default, options: [])
            }
            catch {
                print("Setting category to AVAudioSessionCategoryPlayback failed.")
            }
        }
        playVideo()
    }
    
    // MARK: - AVPlayerViewController
    func playVideo() {
        playerController.player!.play()
    }
    
    func pauseVideo() {
        playerController.player!.pause()
    }
    
    func replayVideo() {
        playerController.player!.seek(to: CMTime.zero)
        playerController.player!.play()
    }
    
    func seekVideo(value: Int) {
        playerController.player!.seek(to: (playerController.player?.currentTime())! + CMTimeMake(value: Int64(value), timescale: 1))
    }
    
    // MARK: - Notification
    @objc private func videoDidEnded() {
        replayVideo()
    }
    
    @objc private func willEnterForegroundNotification() {
        if player != nil {
            locationManager.startUpdatingLocation()
            manager.startDeviceMotionUpdates()
            playVideo()
        }
    }
    
    @objc private func didEnterBackgroundNotification() {
        locationManager.stopUpdatingLocation()
        manager.stopDeviceMotionUpdates()
    }
}

extension ViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !locations.isEmpty {
            let newLocation = locations.first
            if startLocation == nil {
                startLocation = newLocation
            }
            let distanceInMeters = newLocation!.distance(from: startLocation)
            if distanceInMeters > replayVideoStepInMeters {
                startLocation = newLocation
                replayVideo()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        var locationStatus:String?
        
        switch status {
        case CLAuthorizationStatus.restricted:
            locationStatus = "Access: Restricted"
            break
        case CLAuthorizationStatus.denied:
            locationStatus = "Access: Denied"
            break
        case CLAuthorizationStatus.notDetermined:
            locationStatus = "Access: NotDetermined"
            break
        default:
            locationStatus = "Access: Allowed"
            break
        }
        NSLog(locationStatus!)
    }
}

